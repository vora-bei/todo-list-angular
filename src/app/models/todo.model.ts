import * as uuid from 'uuid';

export class Todo {
  id: string;
  title: string;
  completed: boolean;

  constructor(title: string) {
    this.id = uuid.v4();
    this.title = title;
    this.completed = false;
  }
  static copy(todo: Todo){
    const t = new Todo(todo.title);
    t.completed= todo.completed;
    t.id = todo.id;
    return t;
  }
}
