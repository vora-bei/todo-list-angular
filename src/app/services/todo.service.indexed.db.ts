import { Injectable } from '@angular/core';

import { Observable, from, of } from 'rxjs';
import { publishReplay, refCount, map, mergeMap } from 'rxjs/operators';
import { createRxDatabase, RxCollection, addRxPlugin } from 'rxdb';
import adapter from 'pouchdb-adapter-idb';
addRxPlugin(adapter);


import { Todo } from '../models/todo.model';
import { ITodoService } from './todo.service';

const mySchema = {
  keyCompression: false,
  version: 0,
  title: 'todo schema no compression',
  type: 'object',
  properties: {
    id: {
      type: 'string',
      primary: true
    },
    title: {
      type: 'string'
    },
    completed: {
      type: 'boolean'
    }
  },
  required: ['title', 'id']
};

@Injectable()
export class TodoService implements ITodoService {

  db$ = from(createRxDatabase({
    name: 'angular_rxjs_todos',           // <- name
    adapter: 'idb',          // <- storage-adapter
  })).pipe(
    publishReplay(1),
    refCount()
  );
  collections$ = this
    .db$
    .pipe(
      mergeMap(db => db.collections.todo ? of(db.collections) : from(db.addCollections({
        todos: {
          schema: mySchema,
        }
      })
      )),
      publishReplay(1),
      refCount()
    );
  todosCollection$ = this.collections$.pipe(map(collections => collections.todos as RxCollection<Todo>));


  todos$: Observable<Todo[]>;

  isVisible$: Observable<boolean>;

  constructor() {

    this.todos$ = this
      .collections$
      .pipe(
        map(collections => collections.todos as RxCollection<Todo>),
        mergeMap(todos => todos.find().$),
        map(todos => todos.map(t => Todo.copy(t)))
      );

    this.isVisible$ = this.todos$
      .pipe(map((todos) =>
        todos.length === todos.filter(todo => todo.completed).length
      ))

  }

  addTodo(title: string): void {
    this
      .collections$
      .pipe(
        map(collections => collections.todos),
        mergeMap(todos => todos.insert(new Todo(title))),
      )
      .toPromise();
  }

  modify(todo: Todo): void {
    this
      .todosCollection$
      .pipe(mergeMap(todos => from(todos.upsert(todo))))
      .toPromise();
  }

  remove(uuid: string): void {
    this
      .todosCollection$
      .pipe(
        mergeMap(todos => todos.findOne().where('id').eq(uuid).exec()),
        map(todo => from(todo.remove()))
      )
      .toPromise();
  }

  removeCompleted(): void {
    this.todosCollection$.pipe(
      mergeMap(todos => todos.find().where('completed').eq(true).exec()),
      mergeMap(items => from(items)),
      mergeMap(todo => todo.remove()),
    ).toPromise();
  }

  toggle(uuid: string): void {
    this.todosCollection$.pipe(
      mergeMap(todos => todos.findOne().where('id').eq(uuid).exec()),
      mergeMap(todo => from(todo.update({ "$set": { completed: !todo.completed } })))
    ).toPromise();
  }

  toggleAll(completed: boolean): void {
    this.todosCollection$.pipe(
      mergeMap(todos => todos.find().exec()),
      mergeMap(items => from(items)),
      mergeMap(todo => from(todo.update({ "$set": { completed } })))
    ).toPromise();
  }

}
