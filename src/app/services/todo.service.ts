import { Injectable } from '@angular/core';

import { BehaviorSubject, Subject, Observable, Subscription } from 'rxjs';
import { scan, publishReplay, refCount, map } from 'rxjs/operators';


import { Todo } from '../models/todo.model';


type TodosOperation = (todos: Todo[]) => Todo[];

export abstract class ITodoService {

  todos$: Observable<Todo[]>;

  isVisible$: Observable<boolean>;

  
  abstract addTodo(title: string): void ;

  abstract  modify(todo: Todo): void;

  abstract remove(uuid: string): void;

  abstract removeCompleted(): void;

  abstract toggle(uuid: string): void;

  abstract toggleAll(completed: boolean): void;

}
