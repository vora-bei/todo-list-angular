import { ChangeDetectionStrategy, Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators'

import { Todo } from '../../models/todo.model';
import { ITodoService } from '../../services/todo.service';

@Component({
  selector: 'todo-list',
  templateUrl: 'todo-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class TodoListComponent implements OnInit, OnDestroy {

  currentStatus$ = this.route.params.pipe(map(params => params.status));

  isAllCompleted$: Observable<boolean> = this
    .todoService
    .isVisible$;

  visibleTodos$: Observable<Todo[]> = combineLatest([
    this.todoService.todos$,
    this.route.params.pipe(map(params => params.status))
  ])
    .pipe(map(([todos, status]) => {
      switch (status) {
        case 'active':
          return todos.filter(todo => !todo.completed);
        case 'completed':
          return todos.filter(todo => todo.completed);
        default:
          return todos;
      }
    }));
  visibleTodosLength$ = this.visibleTodos$.pipe(map(todos => todos.length));
  constructor(
    private route: ActivatedRoute,
    private todoService: ITodoService
  ) { }

  ngOnInit() {
    // this.todoService.loadPersistTodos();
  }

  ngOnDestroy() {

  }

  remove(uuid: string): void {
    this.todoService.remove(uuid);
  }

  toggle(uuid: string): void {
    this.todoService.toggle(uuid);
  }

  toggleAll(completed: boolean): void {
    this.todoService.toggleAll(completed);
  }

  update(todo: Todo): void {
    this.todoService.modify(todo);
  }

}
