<p align="center">
  <img src="./assets/logo-header.png" alt="Angular RxJS Todos" width="700" />
</p>

# Angular RxJS Todos

This repo shows a basic todos example base on the famous [TodoMVC](https://github.com/tastejs/todomvc) but using RxJS and Angular. The goal is to show how to use the Observables data architecture pattern within Angular 2.

> Fork https://rxjs-cn.github.io/angular-rxjs-todos/). I update angular version. Add new todo service implementation (rxdb based). Change to ChangeDetection.onPush and use pipe async

## Install

If you don't install `angular-cli` before, you need to install it at first:

```shell
npm install -g @angular/cli
```

Make sure you have the `angular-cli`, and then:

```shell

# change into the repo directory
cd angular-rxjs-todos

# install dependencies
npm install

# run
npm start
```

Then visit [http://localhost:4200](http://localhost:4200) in your browser.


## License

MIT
